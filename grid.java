package counter;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class grid extends JPanel {
	public int A,B,C;
	public grid(int a, int b, int c){
		super();
		A = a;
		B = b;
		C = c;
	}
	public void paintComponent(Graphics g)
	{
	    
	    super.paintComponent(g);
		g.setColor(Color.BLACK);
	    int width = A;
	    int height = B;
	    int xstart=C;
        g.drawLine(C, 0, C, B);
	    for(int i = 1; i <= 10; i = i++)
	    {
	        xstart = i*(width/10);
	        g.drawLine(xstart, 0, xstart, height);
	    }
	}
	public static void main(String[] args)
    {
		grid  panel = new grid (80,100, 4);                            
        JFrame application = new JFrame();                            
        
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
                                                                     
        application.add(panel);           


        application.setSize(500, 400);         
        application.setVisible(true);          
    }
}
